package routes

import (
	"olshop/controllers"
	"olshop/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies([]string{"localhost"})

	// CORS Setting
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true
	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")
	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// JWT Setup
	jwt := r.Group("/")
	jwt.Use(middlewares.JwtAuthMiddleware())

	// ROUTES SETUP
	// Auth :
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)
	jwt.POST("/change-password", controllers.ChangePassword)

	jwt.GET("/user-profile", controllers.GetUserProfileById)
	jwt.PATCH("/user-profile", controllers.UpdateUserProfile)

	// swagger routes
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
