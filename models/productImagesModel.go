package models

import "github.com/google/uuid"

type (
	Images struct {
		ID       uuid.UUID `json:"id" gorm:"primary_key"`
		ImageURL string    `json:"images_url" gorm:"type:text" validate:"omitempty"`

		// Relation
		ProductsID uuid.UUID `json:"products_id" gorm:"index"`
		Products   Products  `json:"products" gorm:"foreignKey:ProductsID"`
	}
)
