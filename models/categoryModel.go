package models

import "github.com/google/uuid"

type (
	Category struct {
		ID       uuid.UUID `json:"id" gorm:"primary_key"`
		Category string    `json:"category" gorm:"type:varchar(255);not null" validate:"omitempty,min=1,max=255"`

		// Relation
		Products []Products
	}
)
