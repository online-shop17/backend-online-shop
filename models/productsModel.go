package models

import (
	"time"

	"github.com/google/uuid"
)

type Products struct {
	ID          uuid.UUID `json:"id" gorm:"primary_key"`
	Name        string    `json:"name" gorm:"type:varchar(255)" validate:"omitempty,min=1,max=255"`
	Description string    `json:"description" gorm:"type:text" validate:"omitempty"`
	Quantity    int       `json:"quantity" gorm:"type:int" validate:"omitempty"`
	Price       int       `json:"price" gorm:"type:int" validate:"omitempty"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`

	// Relation
	CategoryID uuid.UUID `json:"category_id" gorm:"index"`
	Category   Category  `json:"category" gorm:"foreignKey:CategoryID"`

	Images    []Images    `json:"images"`
	CartItems []CartItems `json:"cart_items"`
}
