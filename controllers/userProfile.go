package controllers

import (
	"net/http"
	"olshop/models"
	"olshop/utils/token"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

// UpdateUserProfile godoc
// @Summary Update UserProfile.
// @Description Update UserProfile by id.
// @Tags UserProfile
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "UserProfile id"
// @Success 200 {object} models.UserProfile
// @Router /userProfile/{id} [patch]
func UpdateUserProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Extract Token ID User
	userID, _ := token.ExtractTokenID(c)

	// Get model if exist
	var user models.UserProfile
	if err := db.Where("id = ?", userID).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input models.UserProfileInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.UserProfile
	updatedInput.Fullname = input.Fullname
	updatedInput.Phone = input.Phone
	updatedInput.KTP = input.KTP
	updatedInput.Address = input.Address
	updatedInput.City = input.City
	updatedInput.Province = input.Province
	updatedInput.Gender = input.Gender
	updatedInput.ProfileURL = input.ProfileURL

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(updatedInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = db.Model(&user).Updates(updatedInput).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// GetUserProfilById godoc
// @Summary Get UserProfile.
// @Description Get an UserProfile by id.
// @Tags UserProfile
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "UserProfile id"
// @Success 200 {object} models.UserProfile
// @Router /userProfile/{id} [get]
func GetUserProfileById(c *gin.Context) { // Get model if exist
	var user models.User

	// Extract Token ID User
	userID, _ := token.ExtractTokenID(c)

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Preload("UserProfile").Where("id = ?", userID).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})
}
